#!/usr/bin/env node

var DEFAULT_FILE = 'index.html'

console.log("\n## Parsing Arguments ##\n")

var commander = require('commander')

commander
	.description("Generate a blank html file.")
	.option("--force", "write to target file even if it already exists")
	.arguments("[target-file]")
	.action(upgradeArgs)
	.parse(process.argv)

function upgradeArgs(f){file = f}

if (!commander.args.length || !file) {
	console.log("\t (: No file: using `%s`\n", DEFAULT_FILE)
	file = DEFAULT_FILE
}
console.log("target-file : %s", file)

console.log('\n## Generating html code ##\n');

function genHTML()
{
	var jsdom = require('jsdom').jsdom
	var serialize = require('jsdom').serializeDocument

	var doctype = "<!DOCTYPE html>"
	var title   = "<title></title>"
	var script  = "<script></script>"
	var style   = "<style></style>"
	var div     = "<div></div>"
	
	var template = doctype + title + script + style + div
	
	var dom = jsdom(template)
	var source = serialize(dom)

	return require('html').prettyPrint(source)
}

var psource = genHTML()
console.log(psource)

console.log('\n## Writing to %s ## \n', file)

function writeFile() {fs.writeFileSync(file, psource)}

var fs = require('fs')
if (!fs.existsSync(file))
	writeFile()
else
{
	console.log("\t ): File already exists (--force required)")

	if (commander.force)
	{
		console.log(' \t |: Detected: [--force] writing to file')
		writeFile()
	}
	else
	{
		process.exit(1)
	}
}
