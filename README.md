![](resources/coollogo_com-303612620.gif)

# new-html | A cli tool to `touch` with that HTML #

How many times have you written a template for a blank html page?

For me, the answer was way too many.

So, I made this tiny script as my first npm tool.

Now, you can get started from the command line with all of the unflexibibility usually reserved for an IDE.

Rejoice, and move on to real work.

## Installation

`npm install -g new-html`

## Usage

`new-html --help`

yields

```
  Usage: new-html [options] [target-file]

  Generate a blank html file.

  Options:

    -h, --help  output usage information
    --force  write to target file even if it already exists
```

## Output

The target file will be created with this template.

```
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <script></script>
        <style></style>
    </head>
    
    <body>
        <div></div>
    </body>
</html>
```

## TODO

* Evolve into a complicated and cumbersome framework.
* Add cli options to abstain from non-mandatory elements.
* infer file extension when missing.
* add default filename info to `--help`.
* explain safety/`--force` in `--help` description.
* add indent-size option (pass to HTML.pretty)

## Citations

Today's episode was brought to you, in part, by:

* The letter [tutorial](https://developer.atlassian.com/blog/2015/11/scripting-with-node/).
* Too much copy and pasting.
* A computer named Falcore
* And the shoulders of tiny giants.

## Farewell, for now

Ya'll come back now.

:: The Jolly Wizard